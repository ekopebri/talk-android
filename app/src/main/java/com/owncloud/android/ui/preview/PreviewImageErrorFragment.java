/*
 * Nextcloud Talk application
 *
 * @author Mario Danic
 * Copyright (C) 2017-2019 Mario Danic <mario@lovelyhq.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.owncloud.android.ui.preview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.owncloud.android.R;
import com.owncloud.android.ui.fragment.FileFragment;

import static com.owncloud.android.ui.activity.FileActivity.EXTRA_FILE;

/**
 * A fragment showing an error message
 */

public class PreviewImageErrorFragment extends FileFragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.preview_image_error_fragment, container, false);
    }

    public static FileFragment newInstance() {
        FileFragment fileFragment = new PreviewImageErrorFragment();
        Bundle bundle = new Bundle();

        bundle.putParcelable(EXTRA_FILE, null);
        fileFragment.setArguments(bundle);

        return fileFragment;
    }
}
