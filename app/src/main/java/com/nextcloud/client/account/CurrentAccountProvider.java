/*
 * Nextcloud Talk application
 *
 * @author Mario Danic
 * Copyright (C) 2017-2019 Mario Danic <mario@lovelyhq.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nextcloud.client.account;

import android.accounts.Account;

import androidx.annotation.Nullable;

/**
 * This interface provides access to currently selected user Account.
 * @see UserAccountManager
 */
@FunctionalInterface
public interface CurrentAccountProvider {
    /**
     *  Get currently active account.
     *
     * @return Currently selected {@link Account} or first valid {@link Account} registered in OS or null, if not available at all.
     */
    @Nullable
    Account getCurrentAccount();
}
